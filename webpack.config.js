const webpack = require('webpack');
var path = require('path');
const nodeExternals = require('webpack-node-externals');


module.exports = {
  entry: path.resolve(__dirname, './src/server/server.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devServer: {
    host: "0.0.0.0",
    port: "8080",
    contentBase: path.resolve(__dirname, 'dist'),
    hot: true,
  },
  target: 'node', // in order to ignore built-in modules like path, fs, etc.
  externals: [nodeExternals()] // in order to ignore all modules in node_modules folder
};
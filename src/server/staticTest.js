const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

const fs = require('fs');

const axios = require("axios");
 
  const availableKeys = [
    'lr',
    'lc1',
    'lc2',
    'lc3',
    'mf',
    'mq',
    'mt',
    'mr',
    'mp',
    'p',
    'wt',
    'wf',
    'wp',
    'pr',
    'pi',
    'pc',
];

const motorKeys = ['mt' ,'mp' ,'mf' ,'mr' ,'mq'];

const rmcKey2dictionaryKey = {
    'lr': 'metric.runTankLoadcell',
    'lc1': 'metric.engine1',
    'lc2': 'metric.engine2',
    'lc3': 'metric.engine3',
    'mt': 'metric.thrustState',
    'mp': 'metric.purgeState',
    'mf': 'metric.fillState',
    'mr': 'metric.reliefState',
    'mq': 'metric.quickReleaseState',
    'p': 'metric.progress',
    'wt': 'metric.thrustValve',
    'wf': 'metric.fillValve',
    'wp': 'metric.purgeValve',
    'pr': 'metric.runTankPressure',
    'pi': 'metric.injectorPressure',
    'pc': 'metric.combustionChamberPressure',
};

const progressStatus = [
	'NEUTRAL_STATE',
	'VAVLE_CHECK_STATE',
	'FILL_STATE',
	'FIRE_STATE',
	'APPROACH_STATE',
	'ABORT_STATE',
	'READY_STATE',
];

const motorState = [
	'CLOSED',
	'IN_PROGRESS',
	'OPEN',
	'UNKNOWN',
];

function translateValue(key, value) {
    let parsedValue = parseFloat(value);
    if (motorKeys.includes(key)) {
        parsedValue = motorState[parsedValue];
    } else if (key === 'p') {
        parsedValue = progressStatus[parsedValue];
    }
    
    return parsedValue;
}
// function request(line){     
    // console.log(line);
    // const res = axios.post('http://0.0.0.0:5000/insert-data?data=' + encodeURIComponent(line)).catch(err => {
        // console.log(err);
        // console.log(line);
    // });   
// }

function StaticTest() {
    const port = new SerialPort('/dev/ttyACM0', { baudRate: 1000000 });
    const parser = port.pipe(new Readline({ delimiter: '\n' }));

    this.state = {
        'metric.runTankLoadcell': 0,
        'metric.engine1': 0,
        'metric.engine2': 0,
        'metric.engine3': 0,
        'metric.thrustState': 0,
        'metric.purgeState': 0,
        'metric.fillState': 0,
        'metric.reliefState': 0,
        'metric.quickReleaseState': 0,
        'metric.progress': 0,
        'metric.thrustValve': 0,
        'metric.fillValve': 0,
        'metric.purgeValve': 0,
        'metric.runTankPressure': 0,
        'metric.injectorPressure': 0,
        'metric.combustionChamberPressure': 0,
        'metric.totalThrust': 0,
        'metric.maxPressureRuntank': 0,
        'metric.maxPressureInjector': 0,
        'metric.maxPressureCc': 0,
    };

    this.history = {};
    this.listeners = [];
    
    Object.keys(this.state).forEach(function (k) {
        this.history[k] = [];
    }, this);

    /*setInterval(function () {
        this.updateState();
        this.generateTelemetry();
    }.bind(this), 1000); */
    
    parser.on('data', line_ => {
    	const line = (new Date().getTime()) + ": " + line_.replace('\x00', ''); 
        // console.log
        // if (line.length < 50) {
        //     return;
        // }
        fs.appendFile('test.txt', line, function (err) {
        if (err) throw err;
        //   console.log('Saved!');
        });
	
        this.updateState(line);
       this.generateTelemetry();
    });
};

StaticTest.prototype.updateState = function (line_) { 
    const line = line_.replace(' ','').split(','); 

    let data = { 
        'lc1': [],
        'lr': [],
        'lc2': [],
        'lc3': [],
        'mf': [],
        'mq': [],
        'mt': [],
        'mr': [],
        'mp': [],
        'p': [],
        'wt': [],
        'wf': [],
        'wp': [],
        'pr': [],
        'pi': [],
        'pc': []
    }
    // console.log(line);
    for (obj of line) { 
        try {
            const t = obj.split(':');
            // console.log(t, t[0],t[1]);
            if (t[0] in data) { 
                // if (t[0] == 'pc') {
                    // console.log(t[1]);
                // }
                if (t[1] == '') { 
                    data[t[0]].push( data[t[0]][data[t[0]].length - 1]);
                } else {
                    data[t[0]].push(parseFloat(t[1]));
                }                
            }
        } catch (err) {
            console.log(err);
        }
    }  
    // console.log(data);

    const parsedLine = Object.fromEntries(line.reduce((result, currValue) => {
        const [key, value] = currValue.split(':');
        const trimmedKey = key.trim();
        let parsedValue = translateValue(trimmedKey, value);

        if (availableKeys.includes(trimmedKey) && !!parsedValue) {
            const parsedKey = rmcKey2dictionaryKey[trimmedKey];
            return [...result, [parsedKey, parsedValue]];
        }

        return result;
    }, []));

    // console.log(parsedLine);

    this.state = {
        ...this.state,
        ...parsedLine, 
    };
    this.state['metric.totalThrust'] = this.state['metric.engine1']; // + this.state['metric.engine2'] + this.state['metric.engine3'];
    
    // console.log("fille: ", this.state['metric.fillState']);
    // this.state['metric.fillState'] = motorState[this.state['metric.fillState']]; // motorState[Math.floor(Math.random() * 4)];
    // this.state['metric.quickReleaseState'] = motorState[Math.floor(Math.random() * 4)];
    // this.state['metric.thrustState'] = motorState[Math.floor(Math.random() * 4)];
    // this.state['metric.purgeState'] = motorState[Math.floor(Math.random() * 4)];
    // this.state['metric.reliefState'] = motorState[Math.floor(Math.random() * 4)];

    this.state['metric.maxPressureRuntank'] = Math.max(this.state['metric.maxPressureRuntank'], this.state['metric.runTankPressure']);
    this.state['metric.maxPressureInjector'] = Math.max(this.state['metric.maxPressureInjector'], this.state['metric.injectorPressure']);
    this.state['metric.maxPressureCc'] = Math.max(this.state['metric.maxPressureCc'], this.state['metric.combustionChamberPressure']);

    console.log("Update", this.state);
};

/**
 * Takes a measurement of rocket state, stores in history, and notifies 
 * listeners.
 */
StaticTest.prototype.generateTelemetry = function () {
    var timestamp = Date.now();
    var sent = 0;
    
    Object.keys(this.state).forEach(function (id) {
        var state = { timestamp: timestamp, value: this.state[id], id: id};
        this.notify(state);
        this.history[id].push(state);
    }, this);
};

StaticTest.prototype.notify = function (point) {
    this.listeners.forEach(function (l) {
        l(point);
    });
};

StaticTest.prototype.listen = function (listener) {
    this.listeners.push(listener);
    return function () {
        this.listeners = this.listeners.filter(function (l) {
            return l !== listener;
        });
    }.bind(this);
};

module.exports = function () {
    return new StaticTest()
};

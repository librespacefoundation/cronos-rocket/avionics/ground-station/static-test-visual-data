# update your package list
apt update -y
#  upgrade all your installed packages to their latest versions
apt upgrade -y

# Update bootloader EEPROM
rpi-eeprom-update -a

reboot
